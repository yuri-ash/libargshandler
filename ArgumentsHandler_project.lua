-- A project defines one build target
project "ArgumentsHandler"
	location ( "build/" .. action )
	kind "StaticLib"
	language "C++"
	includedirs {
		"include",
	}
	files {
		"include/ArgumentsHandler/**.h","src/**.cpp"
	}
	configuration { "vs2010 or vs2013"}
		buildoptions { "/wd4996" }
		flags { "NoMinimalRebuild", "FatalWarnings"}
	configuration { "gmake or codeblocks" }
		-- workaround bug in gcc 4.8 on ubuntu
		-- includedirs { "/usr/include/x86_64-linux-gnu/c++/4.8/" }
	configuration{}
	
