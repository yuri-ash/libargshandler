#include "ArgumentsHandler/ArgumentsHandler.h"

#include <cstring>

ArgumentsHandler::ArgumentsHandler()
{}

ArgumentsHandler::~ArgumentsHandler()
{
	std::list<Argument*>::iterator currentArgs = m_registerArguments.begin();
	for(; currentArgs != m_registerArguments.end(); ++currentArgs)
	{
		delete (*currentArgs);
	}

	m_registerArguments.clear();

}

void ArgumentsHandler::RegisterArgument(const char* code, ArgType type)
{
	if (!GetArgument(code))
	{
		Argument* newArgument = NULL;
		switch(type)
		{
		case AT_TEXT:
			newArgument = new TextArgument(code);
			break;
		}
		m_registerArguments.push_back(newArgument);
	}
}

Argument* ArgumentsHandler::GetArgument(const char* code)
{
	std::list<Argument*>::iterator currentArgs = m_registerArguments.begin();
	for(; currentArgs != m_registerArguments.end(); ++currentArgs)
	{
		if (!strcmp((*currentArgs)->GetCode(), code))
		{
			return (*currentArgs);
		}
	}

	return NULL;

}

TextArgument* ArgumentsHandler::GetArgumentText(const char* code)
{
	return dynamic_cast<TextArgument*>(GetArgument(code));
}

void ArgumentsHandler::ParseStringArguments(char* argsLine[], const int argsCount)
{
	Argument* currentArgument = NULL;
	for(int i = 1; i < argsCount; ++i)
	{
		Argument* newArgument = GetArgument(argsLine[i]);
		if (newArgument)
		{
			currentArgument = newArgument;
			currentArgument->Select();
		}
		else if (currentArgument)
		{
			currentArgument->AddValue(argsLine[i]);
		}
	}
}
