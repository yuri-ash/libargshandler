#include "ArgumentsHandler/ArgsTemplate.h"

#include <cstring>

Argument::Argument(const char* code):
m_valuesCount(-1)
{
	strncpy(m_code, code, sizeof(m_code));
}

Argument::~Argument()
{}


TextArgument::TextArgument(const char* code):
Argument(code)
{
}

TextArgument::~TextArgument()
{
}

void TextArgument::AddValue(const char* value)
{
	strncpy(m_values[m_valuesCount++], value, sizeof(m_code));
}

