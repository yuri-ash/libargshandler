#ifndef __ARGSTEMPLATE_H__
#define __ARGSTEMPLATE_H__

#if !defined (NULL)
# define NULL 0
#endif

class Argument
{
public:
	Argument(const char* code);
	virtual ~Argument();

	virtual void AddValue(const char* value) = 0;

	inline const char* GetCode(){return m_code;}
	inline void Select(){if (m_valuesCount < 0) m_valuesCount = 0;}
	inline bool IsPresent(){return m_valuesCount >= 0;}
	inline int GetCount(){return m_valuesCount;}

protected:
	int			m_valuesCount;
	char		m_code[64];
};

class TextArgument : public Argument
{
public:
	TextArgument(const char* code);
	virtual ~TextArgument();

	virtual void AddValue(const char* value);
	inline const char* GetValue(int index){return index < m_valuesCount ? m_values[index] : NULL;}

protected:
	char	m_values[16][1024];
};

#endif //__ARGSTEMPLATE_H__
