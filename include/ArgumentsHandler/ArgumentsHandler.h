#ifndef __ARGUMENTSHANDLER_H__
#define __ARGUMENTSHANDLER_H__

#include "ArgsTemplate.h"

#include <list>

class ArgumentsHandler
{
public:
	enum ArgType
	{
		AT_TEXT		= 0
	};

public:
	ArgumentsHandler();
	~ArgumentsHandler();

	void RegisterArgument(const char* code, ArgType type);
	Argument* GetArgument(const char* code);
	TextArgument* GetArgumentText(const char* code);

	void ParseStringArguments(char* argsLine[], const int argsCount);

private:
	std::list<Argument*>	m_registerArguments;

};

#endif //__ARGUMENTSHANDLER_H__
